# Python Flask_restful API

### Project details
Python Flask_restful API project is mini API version of goodreads.com. This python project focuses on Building API with Flask. 
It has list of books that can be added/updated/deleted. This list is created and maintained by Admin user. Admin is user that has certain powers to add/delete/update list of books.

>**Note:** Flask wtforms are not used. This is entirely API project and does not focus on the frontend. However, if frontend is added some other language shall be used.

### Aspects covered are as below -
Every branch/tag covers the aspsect that are covered in the Project. 

This project touches on different topic of Flask API development - 
* Building API with flask-restful 
* Application factory pattern
* Using SQLAlchmey 
* Flask Blueprints
* Using Connexion with swagger for API documentation 

| Features | tag | branch |
|----------|----------|----------|
| GET, POST, DELETE, UPDATE using simple flask-restful   | v0.1-flask-restful-basic-latest  | v0.1 |
| flask_sqlalchemy with factory pattern   | v0.2-sql-alchemy-latest | v0.2 |
| flask_bluepring | v0.3-flask-bp-latest | v0.3 |


>**Tested On -** Gitpod - python 3.12.1

#### API Detils - 

| Book API url  | Operations | Details | User |
|----------|----------|----------|----------|
|  127.0.0.1:5000/  | GET | takes you home page | Any |
|  127.0.0.1:5000/api/v1/resources/books/all  | GET | get list of all books | Any |
|  127.0.0.1:5000/api/v1/resources/books/1  | GET | get book by id from bookslist | Any |
|  127.0.0.1:5000/api/v1/resources/books/1  | GET, POST, PUT, DELETE| operations on book by| Admin |

| User API url  | Operations | Details | User |
|----------|----------|----------|----------|
| /api/v1/resources/users   | GET | get list of all users | Admin |
| /api/v1/resources/users/login  | GET | perform login | Registered user |
| /api/v1/resources/users/register   | GET | register user | Any |


#### DB Details - 

At the moment ```sqlite``` database is used, which consist of only 1 table for the Books. 

Tables created for this application - 

**Bookslist table -** Contains list of all the books. 
| id | title  | author | first_sentence | year_published | comments |
|----------|----------|----------|----------|----------|----------|

**BookReviews table -** Contains comments/reviews of all the books.
| id  | review_content  | book_id |
|----------|----------|----------|


**Directory structure -**
```
python_flask_api_basic/
│
├── README.md
├── .gitignore
├── requirements.txt
│
└── src/
	└── wsgi.py
    └──api
       |
       └── resources
       |    └── book_resource.py
       └── database
       |    └── database_settings.yaml	
       |    └── dummy_data.yaml	
       |    └── models.yaml	
       |    └── __init__.yaml	
       |
       └── config.py
       └── utils.py
	   └── __init__.py
	
	
```

### Running application - 

Before starting install and activate ```venv```
```
# python3 -m venv venv
# source venv/bin/activate
# pip install -r requirements.txt 
#
# cd /src
# python wsgi.py         # Starting the application 
```

Each branch/tag is a release that has covered one of the aspect. 
Please checkout to the branch for every release. Each branch has it's own README.md that provides more information about the release and aspects covered.
```
# git checkout v1.1
``` 

### Important links 
* [Flask restful Sample code](https://github.com/bryaneaton/flask-restful)
* [Flask restful example code](https://github.com/erdem/flask-restful-example/tree/master)
* [FLask Mega Tutorials](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xv-a-better-application-structure)
* [Flask with SQLAlchmey + Migration](https://github.com/enzoftware/flask-rest-api)
* [Flask Videos by Corey](https://www.youtube.com/watch?v=MwZwr5Tvyxo&list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH&index=1)
* [Flask real world example](https://github.com/gothinkster/flask-realworld-example-app)
